class Options {
    constructor(player) {
      this.player = player,
      this._bot = ""
    }
  
    set bot(bot) {
      this._bot = bot;
    }
  
    get bot() {
      return this._bot;
    }
  
    setBot(bot){
      this._bot = bot;
    }
  
    getBot(){
      return this._bot;
    }
  
    #isRandom() {
      let bot = ['kertas', 'batu', 'gunting'];
      return bot[Math.floor(Math.random() * bot.length)]; 
    }
  
    getIsRandom(){
      return this.#isRandom();
    }
  
    #isRule() {
      let player = this.player;
      this.getBot();
      let botPick = this._bot;
  
      if(
        (player === 'kertas' && botPick === 'batu') || 
        (player === "batu" && botPick === "gunting") || 
        (player === "gunting" && botPick === "kertas")) {
          return "win";
      } else if (player === botPick) {
        return "draw";
      } else {
        return "lose";
      }
    }
  
    getIsRule(){
      return this.#isRule();
    }
  }
  
  class Gameplay extends Options {
    constructor(player,winner) {
      super(player);
      this.winner = winner
    }
  
    play() {
      return this.getIsRule();
    }
  
    matchResult(){
      if (this.getIsRule() === "win"){
        console.log(`player: ${this.player} bot: ${this._bot} | player menang`)
      } else if (this.getIsRule() === "lose"){
        console.log(`player: ${this.player} bot: ${this._bot} | bot menang`);
      } else {
        console.log(`player: ${this.player} bot: ${this._bot} | yah imbang`)
      }
      return this.winner;
    }
  }
  
  let game = new Gameplay();
  document.getElementById("Player").addEventListener("click",gameStart);
  document.getElementById("refresh").addEventListener("click",refreshGame);
  
  function gameStart(props) {
    if (
    props.target.id === "batu" || 
    props.target.id === "gunting" || 
    props.target.id === "kertas") {
        document.getElementById(props.target.id).classList.toggle("activePick");
        game.player = props.target.id;
        game.setBot(game.getIsRandom());
        document.getElementById(`bot${game.getBot()}`).classList.add("activePick");
        document.getElementById(game.getIsRule()).style.visibility = "visible";
        document.getElementById("vs").style.visibility = "hidden";
        game.matchResult();
        if (game.player != null) {
          document.getElementById("Player").removeEventListener("click",gameStart);
          document.getElementById("Player").addEventListener("click",pleaseRefresh);
        }
    }
  }
  
  function pleaseRefresh() {
    alert("Please refresh this game");
  }
  
  function refreshGame(){
    document.getElementById("Player").removeEventListener("click",pleaseRefresh);
    document.getElementById("Player").addEventListener("click",gameStart);
    document.getElementById(game.player).classList.remove("activePick");
    document.getElementById(`bot${game.getBot()}`).classList.remove("activePick");
    document.getElementById(game.getIsRule()).style.visibility = "hidden";
    document.getElementById("vs").style.visibility = "visible";
  }